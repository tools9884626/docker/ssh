FROM alpine:3.15.6

LABEL Maintainer="PapierPain <papierpain4287@outlook.fr>"
LABEL Description="Alpine container with ssh configuration"

RUN apk add --no-cache --update openssl openssh sshpass
